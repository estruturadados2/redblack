#include <stdio.h>
#include <stdlib.h>

#define RED   1
#define BLACK 0

typedef struct arvoreRB {
  int info;
  int cor;
  struct arvoreRB *esq;
  struct arvoreRB *dir;
  struct ArvoreRB *top;
} ArvoreRB;

int eh_no_vermelho(ArvoreRB * no){
  if(!no) return BLACK;
  return(no->cor == RED);
}

int buscar (ArvoreRB *a, int v) {
  if (a == NULL) { return 0; } /*Nao achou*/
  else if (v < a->info) {
    return buscar (a->esq, v);
  }
  else if (v > a->info) {
    return buscar (a->dir, v);
  }
  else { return 1; } /*Achou*/
}

void in_order(ArvoreRB *a){
  if(!a)
    return;
  in_order(a->esq);
  printf("%d ",a->info);
  in_order(a->dir);
}

void print(ArvoreRB * a,int spaces){
  int i;
  for(i=0;i<spaces;i++) printf(" ");
  if(!a){
    printf("//\n");
    return;
  }

  printf("%d\n", a->info);
  print(a->esq,spaces+2);
  print(a->dir,spaces+2);
}

void rotateDir(ArvoreRB * base){
    ArvoreRB * temp = base->esq->dir;
    ArvoreRB * esq = base->esq;
    int infbse = base->info;
    base->info = esq->info;
    esq->info = infbse;

    esq->dir = base->dir;
    base->esq = esq->esq;
    esq->esq = temp;
    base->dir = esq;

    infbse = esq->cor;
    esq->cor = base->cor;
    base->cor = infbase;
}

void rotateEsq(ArvoreRB * base){
    ArvoreRB * temp = base->dir->esq;
    ArvoreRB * dir = base->dir;
    int infbse = base->info;
    base->info = dir->info;
    dir->info = infbse;

    dir->esq = base->esq;
    base->dir = dir->dir;
    dir->dir = temp;
    base->esq = dir;

    infbse = dir->cor;
    dir->cor = base->cor;
    base->cor = infbase;
}

void inserirAux(ArvoreRB * base){
    while(base->top->cor == RED){
        if(base->top == base->top->top->dir){
            ArvoreRB * tmp = base->top->top->esq;
            if(tmp->cor == RED){
                tmp->cor = BLACK;
                base->top->cor = BLACK;
                base->top->top->cor = RED;
                base = base->top->top;
            }else if(base == base->top->esq){
                base = base->top;
                rotateEsq(base);
                base->top->cor = BLACK;
                base->top->top->cor = RED;
                rotateDir(base);
            }
        }else{
            ArvoreRB * tmp = base->top->top->dir;
            if(tmp->cor == RED){
                tmp->cor = BLACK;
                base->top->cor = BLACK;
                base->top->top->cor = RED;
                base = base->top->top;
            }else if(base == base->top->dir){
                base = base->top;
                rotateEsq(base);
                base->top->cor = BLACK;
                base->top->top->cor = RED;
                rotateDir(base);
            }
        }
    }

    base->cor = BLACK;
}

ArvoreRB * inserir(ArvoreRB * base, ArvoreRB *a, int v) {
  if (a == NULL) {
    a = (ArvoreRB*)malloc(sizeof(Arvore));
    a->info = v;
    a->esq = a->dir = NULL;
    a->cor = RED;
    a->top = base;
    inserirAux(ArvoreRB * a);
  }else if (v < a->info) {
    a->esq = inserir (base, a->esq, v);
  }else {
    a->dir = inserir (base, a->dir, v);
  }
  return a;
}

int main(){
  ArvoreRB * a;

}

